<?php
/**
 * UI Fields plugin for Craft CMS
 *
 * UI Fields Translation
 *
 * @author    Tipping Media LLC
 * @copyright Copyright (c) 2016 Tipping Media LLC
 * @link      http://tippingmedia.com
 * @package   UiFields
 * @since     1.0.0
 */

return array(
    'Translate me' => 'To this',
);
