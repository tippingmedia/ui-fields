<?php
/**
 * UI Fields plugin for Craft CMS
 *
 * UiFields_Range FieldType
 *
 * --snip--
 * Whenever someone creates a new field in Craft, they must specify what type of field it is. The system comes with
 * a handful of field types baked in, and we’ve made it extremely easy for plugins to add new ones.
 *
 * https://craftcms.com/docs/plugins/field-types
 * --snip--
 *
 * @author    Tipping Media LLC
 * @copyright Copyright (c) 2016 Tipping Media LLC
 * @link      http://tippingmedia.com
 * @package   UiFields
 * @since     1.0.0
 */

namespace Craft;

class UiFields_RangeFieldType extends BaseFieldType
{
    /**
     * Returns the name of the fieldtype.
     *
     * @return mixed
     */
    public function getName()
    {
        return Craft::t('Range Slider');
    }

    /**
     * Returns the content attribute config.
     *
     * @return mixed
     */
    public function defineContentAttribute()
    {
        return AttributeType::Number;
    }

    /**
     * Defines the settings.
     *
     * @access protected
     * @return array
     */
    protected function defineSettings()
    {

        $settings['defaultRange']  = AttributeType::Number;
        $settings['minimum'] = AttributeType::Number;
        $settings['maximum'] = AttributeType::Number;
        $settings['step'] = AttributeType::Number;

        return $settings;
    }


    /**
     * Returns the field's input HTML.
     *
     * @param string $name
     * @param mixed  $value
     * @return string
     */
    public function getInputHtml($name, $value)
    {
        if (!$value)
            $value = new UiFields_RangeModel();

        $id = craft()->templates->formatInputId($name);
        $namespacedId = craft()->templates->namespaceInputId($id);
        $settings = $this->getSettings();

/* -- Include our Javascript & CSS */

        craft()->templates->includeCssResource('uifields/css/fields/UiFields_RangeFieldType.css');
        craft()->templates->includeJsResource('uifields/js/fields/UiFields_RangeFieldType.js');

/* -- Variables to pass down to our field.js */

        $jsonVars = array(
            'id' => $id,
            'name' => $name,
            'namespace' => $namespacedId,
            'prefix' => craft()->templates->namespaceInputId(""),
            );

        $jsonVars = json_encode($jsonVars);
        craft()->templates->includeJs("$('#{$namespacedId}-field').UiFields_RangeFieldType(" . $jsonVars . ");");

/* -- Variables to pass down to our rendered template */

        $variables = array(
            'id' => $id,
            'name' => $name,
            'namespaceId' => $namespacedId,
            'values' => $value,
            'settings' => $settings
            );

        return craft()->templates->render('uifields/fields/UiFields_RangeFieldType.twig', $variables);
    }

    /**
     * Returns the field's settings HTML.
     *
     * @return string|null
     */
    public function getSettingsHtml()
    {
        return craft()->templates->render('uifields/fields/UiFields_RangeSettings',array(
            'settings' => $this->getSettings()
        ));
    }

    /**
     * Returns the input value as it should be saved to the database.
     *
     * @param mixed $value
     * @return mixed
     */
    public function prepValueFromPost($value)
    {
        //\CVarDumper::dump($value, 5, true);exit;
        return $value['range'];
    }

    /**
     * Prepares the field's value for use.
     *
     * @param mixed $value
     * @return mixed
     */
    public function prepValue($value)
    {
        return $value;
    }
}
