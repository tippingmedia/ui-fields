/**
 * UI Fields plugin for Craft CMS
 *
 * UiFields_RangeFieldType JS
 *
 * @author    Tipping Media LLC
 * @copyright Copyright (c) 2016 Tipping Media LLC
 * @link      http://tippingmedia.com
 * @package   UiFields
 * @since     1.0.0
 */

 ;(function ( $, window, document, undefined ) {

    var pluginName = "UiFields_RangeFieldType",
        defaults = {
        };

    // Plugin constructor
    function Plugin( element, options ) {
        this.element = element;

        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;

        this.init(element);
    }

    Plugin.prototype = {

        init: function(id) {
            var _this = this,
                field = $(id),
                slider = field.find('.range-slider'),
                range = field.find('.range-slider__range'),
                value = field.find('.range-slider__value');

            $(function () {
/* -- _this.options gives us access to the $jsonVars that our FieldType passed down to us */
                slider.each(function(){
                    value.each(function(){
                        var value = $(this).prev().val();
                        $(this).html(value);
                    });

                    range.on('input', function(){
                        $(this).next(value).html(this.value);
                    });
                });

            });
        }
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );
