# UI Fields plugin for Craft CMS

## Installation

To install UI Fields, follow these steps:

1. Download & unzip the file and place the `uifields` directory into your `craft/plugins` directory
2.  -OR- do a `git clone ???` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3. Install plugin in the Craft Control Panel under Settings > Plugins
4. The plugin folder should be named `uifields` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

UI Fields works on Craft 2.4.x and Craft 2.5.x.

## UI Fields Overview

UIFields is a plugin of various field types.
1. Columns - This field is a graphical input to select number of columns. Settings include number of maximum columns & default number of columns.
2. Range - This field is a graphical range input.  It allows you to select a number based on range or step count. Settings include, default range, max range, min range & range step.


## Using UI Fields

Create a field and select either "Columns" or "Range Slider".

## UI Fields Roadmap

Some things to do, and ideas for potential features:

* Release it
* Add more graphical fields

## UI Fields Changelog

### 1.0.0 -- 2016.07.07

* Initial release

Brought to you by [Tipping Media LLC](http://tippingmedia.com)
