<?php
/**
 * UI Fields plugin for Craft CMS
 *
 * UiFields_Columns Model
 *
 * --snip--
 * Models are containers for data. Just about every time information is passed between services, controllers, and
 * templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 * --snip--
 *
 * @author    Tipping Media LLC
 * @copyright Copyright (c) 2016 Tipping Media LLC
 * @link      http://tippingmedia.com
 * @package   UiFields
 * @since     1.0.0
 */

namespace Craft;

class UiFields_ColumnsModel extends BaseModel
{
    /**
     * Defines this model's attributes.
     *
     * @return array
     */
    protected function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), array(
            'columns'     => array(AttributeType::Number, 'default' => '3')
        ));
    }

}
